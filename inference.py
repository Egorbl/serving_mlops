import sys
import logging as log
from pathlib import Path

import cv2
from ultralytics import YOLO

sys.path.append(str(Path(__file__).resolve().parents[1].joinpath('configs')))

NCNN_MODEL_DET = YOLO("best.pt", 'detect')


def detect_get_bounding_boxes(input, raw_output=False):
    """
    Performs NCNN inference on input. Can return raw boxes coodrinates
    (use raw_output flag) or rectangles as bounding boxes.
    Params:
        input: image. np.ndarray
    Returns:
        input_image: The combined image. np.ndarray
    """

    log.info('Inference: detection')
    results = NCNN_MODEL_DET.simple_predict(input)
    out_boxes = []
    rects = []

    for result in results:
        boxes = result.boxes.cpu().numpy()
        out_boxes += boxes
        for box in boxes:
            rects.append(box.xyxy[0].astype(int))

    log.info(F'Detected {len(rects)} objects')
    if raw_output:
        return out_boxes

    return rects

def detect_draw_boxes(input_image):
    """
    Combines image and its detected bounding boxes into a single image.
    Params:
        input_image: image. np.ndarray
    Returns:
        input_image: The combined image. np.ndarray
    """
    log.info('Drawing bounding boxes')
    rects = detect_get_bounding_boxes(input_image)
    for r in rects:
        cv2.rectangle(input_image, r[:2], r[2:], (255, 255, 255), 2)
    return input_image
