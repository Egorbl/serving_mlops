from ultralytics import YOLO
import streamlit as st
import cv2
from pytube import YouTube
import io

def detect_get_bounding_boxes(input, raw_output=False):
    """
    Performs NCNN inference on input. Can return raw boxes coodrinates
    (use raw_output flag) or rectangles as bounding boxes.
    Params:
        input: image. np.ndarray
    Returns:
        input_image: The combined image. np.ndarray
    """

    model = load_model()
    results = model.predict(input)
    out_boxes = []
    rects = []

    for result in results:
        boxes = result.boxes.cpu().numpy()
        out_boxes += boxes
        for box in boxes:
            rects.append(box.xyxy[0].astype(int))

    if raw_output:
        return out_boxes

    return rects


def load_model():
    """
    Loads a YOLO object detection model from the specified model_path.

    Parameters:
        model_path: The path to the YOLO model file.

    Returns:
        A YOLO object detection model.
    """
    model = YOLO("best.pt")
    return model


def _display_detected_frames(st_frame, image):
    """
    Display the detected objects on a video frame using the YOLOv8 model.

    Args:
    - conf (float): Confidence threshold for object detection.
    - model (YoloV8): A YOLOv8 object detection model.
    - st_frame (Streamlit object): A Streamlit object to display the detected video.
    - image (numpy array): A numpy array representing the video frame.
    - is_display_tracking (bool): A flag indicating whether to display object tracking (default=None).

    Returns:
    None
    """
    # Resize the image to a standard size
    image = cv2.resize(image, (720, int(720 * (9 / 16))))

    boxes = detect_get_bounding_boxes(image)

    # # Plot the detected objects on the video frame
    res_plotted = plot_detect(image, boxes)
    st_frame.image(res_plotted,
                   caption='Detected Video',
                   channels="BGR",
                   use_column_width=True
                   )
    return res_plotted


def play_youtube_video(*args, **kwargs):
    """
    Plays a webcam stream. Detects Objects in real-time using the YOLOv8 object detection model.

    Parameters:
        conf: Confidence of YOLOv8 model.
        model: An instance of the `YOLOv8` class containing the YOLOv8 model.

    Returns:
        None

    Raises:
        None
    """
    source_youtube = st.sidebar.text_input("YouTube Video url")

    if st.sidebar.button('Detect Objects'):
        try:
            yt = YouTube(source_youtube)
            stream = yt.streams.filter(file_extension="mp4", res=720).first()
            vid_cap = cv2.VideoCapture(stream.url)

            st_frame = st.empty()
            while vid_cap.isOpened():
                success, image = vid_cap.read()
                if success:
                    _display_detected_frames(st_frame,
                                             image)
                else:
                    vid_cap.release()
                    break
        except Exception as e:
            st.sidebar.error("Error loading video: " + str(e))


def play_stored_video(*args, **kwargs):
    """
    Plays a stored video file. Tracks and detects objects in real-time using the YOLOv8 object detection model.

    Parameters:
        conf: Confidence of YOLOv8 model.
        model: An instance of the `YOLOv8` class containing the YOLOv8 model.

    Returns:
        None

    Raises:
        None
    """
    # source_vid = st.sidebar.selectbox(
    #     "Choose a video...", settings.VIDEOS_DICT.keys())
    uploaded_file = st.file_uploader("Choose a video...", type=["mp4", "avi"])
    if uploaded_file:
        video_bytes = io.BytesIO(uploaded_file.read())
        temporary_location = "videos/video"

        with open(temporary_location, 'wb') as out:
            out.write(video_bytes.read())

        out.close()

    # with open(settings.VIDEOS_DICT.get(source_vid), 'rb') as video_file:
    #     video_bytes = video_file.read()

    if st.sidebar.button('Detect Video Objects') and uploaded_file:
        try:
            vid_cap = cv2.VideoCapture(temporary_location)
            st_frame = st.empty()
            while vid_cap.isOpened():
                success, image = vid_cap.read()
                if success:
                    _display_detected_frames(st_frame,
                                             image)
                else:
                    vid_cap.release()
                    break
        except Exception as e:
            st.sidebar.error("Error loading video: " + str(e))


def plot_detect(image, bboxs_xyxy=None):
    res_image = image.copy()

    if bboxs_xyxy is None:
        return res_image

    for bbox in bboxs_xyxy:
        cv2.rectangle(res_image, (bbox[0], bbox[1]), (bbox[2], bbox[3]), (0, 255, 0), 1)

    return res_image


